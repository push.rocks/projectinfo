import { tap, expect } from '@pushrocks/tapbundle';
import * as smartpath from '@pushrocks/smartpath';
import * as projectinfo from '../ts/index.js';

import * as path from 'path';
let testBasePath = path.resolve(smartpath.get.dirnameFromImportMetaUrl(import.meta.url));

let myNpm = new projectinfo.ProjectinfoNpm(testBasePath, { gitAccessToken: 'sometoken' });
tap.test('should have .packageJson', async () => {
  expect(myNpm.packageJson).property('version').toEqual('1.0.0');
  expect(myNpm.packageJson).property('name').toEqual('testpackage');
});

tap.test('should have .version', async () => {
  expect(myNpm).property('version').toEqual('1.0.0');
});

tap.test('should have .name', async () => {
  expect(myNpm).property('name').toEqual('testpackage');
});

tap.test('should have .license', async () => {
  expect(myNpm).property('license').toEqual('MIT');
});
tap.test('should have .git', async () => {
  expect(myNpm.git.httpsUrl).toEqual('https://sometoken@github.com/someuser/somerepo.git');
});
tap.test('should return a name', async () => {
  expect(projectinfo.getNpmNameForDir(testBasePath)).toEqual('testpackage');
});

tap.start();
